# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from alibaba.models import PartnersAlibabaProductDraft


# class AlibabaItem(scrapy.Item):
#     # define the fields for your item here like:
#     # name = scrapy.Field()
#     pass


class AlibabaItem(DjangoItem):
    django_model = PartnersAlibabaProductDraft
