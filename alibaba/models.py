from django.db import models


class PartnersAlibabaProductDraft(models.Model):
    id = models.BigIntegerField(primary_key=True, )
    sku = models.BigIntegerField()
    mpn = models.CharField(max_length=20)
    name = models.CharField(max_length=1000)
    url = models.URLField(null=True)
    brand_name = models.JSONField(null=True)
    product_description_html = models.TextField(null=True)
    prices = models.JSONField(null=True)
    colors = models.JSONField(null=True)
    production = models.JSONField(null=True)
    quick_details = models.JSONField(null=True)
    specification = models.JSONField(null=True)
    photos = models.JSONField(null=True)
    video = models.URLField(null=True)

    def __str__(self):
        return self.name
