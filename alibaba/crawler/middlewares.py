# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
from scrapy.http import HtmlResponse
from scrapy_selenium import SeleniumRequest
from scrapy_selenium.middlewares import SeleniumMiddleware
from selenium.webdriver.support.wait import WebDriverWait


class AlibabaSeleniumMiddleware(SeleniumMiddleware):
    def process_request(self, request, spider):
        """Process a request using the selenium driver if applicable"""

        if not isinstance(request, SeleniumRequest):
            return None

        self.driver.get(request.url)

        for cookie_name, cookie_value in request.cookies.items():
            self.driver.add_cookie(
                {
                    'name': cookie_name,
                    'value': cookie_value
                }
            )

        # why script execute after waiting in stock?
        if request.script:
            self.driver.execute_script(request.script)

        # we need until_not
        if request.wait_until:
            WebDriverWait(self.driver, request.wait_time).until_not(
                request.wait_until
            )

        if request.screenshot:
            request.meta['screenshot'] = self.driver.get_screenshot_as_png()


        body = str.encode(self.driver.page_source)

        # Expose the driver via the "meta" attribute
        request.meta.update({'driver': self.driver})

        return HtmlResponse(
            self.driver.current_url,
            body=body,
            encoding='utf-8',
            request=request
        )