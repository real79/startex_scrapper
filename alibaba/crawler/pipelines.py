# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from alibaba.models import PartnersAlibabaProductDraft


class AlibabaPipeline(object):
    def process_item(self, item, spider):
        PartnersAlibabaProductDraft.objects.create(
            id=item['id'],
            sku=item['sku'],
            mpn=item['mpn'],
            name=item['name'],
            brand_name=item['brand_name'],
            product_description_html=item['product_description_html'],
            prices=item['prices'],
            colors=item['colors'],
            production=item['production'],
            quick_details=item['quick_details'],
            specification=item['specification'],
            url=item['url'],
            photos=item['photos'],
            video=item['video']
        )
        return item
