# -*- coding: utf-8 -*-
import scrapy
from json import loads
import requests
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import os
from selectorlib import Extractor, Formatter
import re
import logging
from transliterate.decorators import transliterate_function
from alibaba.crawler.items import AlibabaItem


@transliterate_function(language_code='ru', reversed=True)
def translit(text):
    return text


logging.getLogger("selenium").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


class Price(Formatter):
    def format(self, text: str):
        text = text.replace(',', '.')
        price = re.findall(r'\d+\.\d+', text)
        if price:
            return price[0]
        return None

    @staticmethod
    def get_two_prices(text: str):
        text = text.replace(',', '.')
        price = re.findall(r'\d+\.\d+', text)
        if len(price) > 0:
            return price[0], price[1] if len(price) > 1 else None
        return None


class AlibabaCrawlerSpider(scrapy.Spider):
    name = 'alibaba'
    allowed_domains = ['alibaba.com']
    start_urls = [
        'https://russian.alibaba.com/trade/search?spm=a27aq.13929326.IndustryCategory.2.5219393b5O16XC&CateId=100000266'
        '&SearchText=%D0%A2%D0%BA%D0%B0%D0%BD%D0%B8&language=ru&indexArea=product_en']
    search_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/search_results.yml"))
    product_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/product_details.yml"))
    color_div_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/color_div.yml"))
    color_span_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/color_span.yml"))
    price_li_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/price_li.yml"), formatters=[Price])
    price_cpan_extractor = Extractor.from_yaml_file(
        os.path.join(os.path.dirname(__file__), "../resources/price_cpan.yml"))
    logger = logging.getLogger()

    page = 1
    max_pages = 0

    def start_requests(self):
        """Read keywords from keywords file amd construct the search URL"""

        url = "https://russian.alibaba.com/trade/search?spm=a27aq.13929326.IndustryCategory.2.5219393b5O16XC&CateId=100000266&SearchText=%D0%A2%D0%BA%D0%B0%D0%BD%D0%B8&language=ru&indexArea=product_en"
        # The meta is used to send our search text into the parser as metadata
        yield SeleniumRequest(url=url,
                              callback=self.parse,
                              wait_time=5,
                              script="window.scrollTo(0, document.body.scrollHeight);",
                              wait_until=EC.visibility_of_element_located(
                                  (By.XPATH, './/i[contains(@class, "ui2-icon-loading")]'))
                              )

    def parse_product_details(self, response):
        url_english = f'https:{response.text.split("english=")[1].split(",")[0]}'
        data = self.product_extractor.extract(response.text, base_url=response.url)
        product = AlibabaItem()
        product['id'] = data['id']
        product['sku'] = data['id']
        product['mpn'] = f'{data["id"]}-00'
        product['brand_name'] = {
                '@type': 'Brand',
                'name': 'Ali'
            }
        product['name'] = data['name']
        product['url'] = response.url
        product['product_description_html'] = data['product_description1'] if data['product_description1'] else data[
            'product_description2']
        product['photos'] = [''.join(photo.split('_')[:-1]) for photo in data['photos']]
        product['video'] = data['video']

        try:
            if '<li data-role="ladder-price-item"' in data['prices']:
                prices = self.price_li_extractor.extract(data['prices'])
                prices_dict = {f'price_{idx + 1}': i['price'] for idx, i in enumerate(prices['prices'])}
                prices_dict.update({f'threshold_{idx + 1}': i['threshold'] for idx, i in enumerate(prices['prices'])})
            else:
                prices = self.price_cpan_extractor.extract(data['prices'])
                price2, price1 = Price.get_two_prices(prices['price'])
                if price1:
                    prices_dict = {
                        'price_1': price1,
                        'threshold_1': f'<{prices["threshold"]}',
                        'price_2': price2,
                        'threshold_2': f'>={prices["threshold"]}',
                    }
                else:
                    prices_dict = {'price_1': price1}
            product['prices'] = prices_dict
        except Exception as error:
            self.logger.error(f'Ошибка разбора цен в {response.url}')
            self.logger(error.with_traceback())
            product['prices'] = None

        try:
            if data['colors']:
                if data['colors'][0].startswith('<span'):
                    if '<img' in data['colors'][0]:
                        colors = {f'color_name{idx + 1}': self.color_span_extractor.extract(i)["photo_name"]
                                  for idx, i in enumerate(data['colors'])}
                        colors.update(
                            {f'color_photo{idx + 1}': self.color_span_extractor.extract(i)["photo"]
                             for idx, i in enumerate(data['colors'])}
                        )
                    else:
                        colors = {f'color_name{idx + 1}': self.color_span_extractor.extract(i)["name"]
                                  for idx, i in enumerate(data['colors'])}
                else:
                    colors = {f'color_name{idx + 1}': self.color_div_extractor.extract(i)["name"]
                              for idx, i in enumerate(data['colors'])}
                    colors.update(
                        {f'color_photo{idx + 1}': self.color_div_extractor.extract(i)["photo"]
                         for idx, i in enumerate(data['colors'])}
                    )
            else:
                colors = None
            product['colors'] = colors
        except Exception as error:
            self.logger.error(f'Ошибка разбора цветов в {response.url}')
            self.logger(error.with_traceback())
            product['colors'] = None

        try:
            if data['production']:
                product['production'] = {f'threshold_to_production{idx+1}': i
                              for idx, i in enumerate(data['production']['thresholds'][1:])}
                product['production'].update(
                    {f'days_to_production{idx+1}': i
                     for idx, i in enumerate(data['production']['days'][1:])}
                )
            else:
                product['production'] = None
        except Exception as error:
            self.logger.error(f'Ошибка разбора сроков изготовления в {response.url}')
            self.logger(error.with_traceback())
            product['production'] = None

        try:
            english = {item['attrNameId']: item['attrName'] for item in filter(lambda x: x.get('attrNameId', None),
                                                                               loads(
                                                                                   requests.get(url_english).text.split(
                                                                                       '"productBasicProperties":')[
                                                                                       1].split(']')[0] + ']'))}
        except Exception as error:
            print(f'Ошибка запроса(requests) в {url_english}')
            print(error.with_traceback())

        try:
            product['quick_details'] = {english[i['attrNameId']] if i.get('attrNameId')
                                        else translit(i['attrName']): i['attrValue'] for i in
                                        loads(requests.get(response.url).text.split('"productBasicProperties":')[1].
                                              split(']')[0] + ']')}
        except Exception as error:
            print(f'Ошибка запроса(requests) в {response.url}')
            print(error.with_traceback())

        if data['specification1']:
            product['specification'] = {f'spec_{translit(i["name"])}': i['value'] for i in data['specification1']}
        elif data['specification2']:
            product['specification'] = {f'spec_{translit(i["name"])}': i['value'] for i in data['specification2']}
        else:
            product['specification'] = None

        yield product

    def parse(self, response):
        data = self.search_extractor.extract(response.text, base_url=response.url)
        if data['products']:
            for product in data['products']:
                if product['link']:  # Может быть реклама, там линка нет
                    with open('links', 'a') as f:
                        f.write(f"{product['link']}\n")
                    yield SeleniumRequest(url=product['link'], callback=self.parse_product_details)

            # Try paginating if there is data
            self.page += 1
            if self.max_pages == 0 or self.max_pages > self.page:
                if self.page == 2:
                    url = f'{response.request.url}&page=2'
                else:
                    url = re.sub('(^.*?&page\=)(\d+)(.*$)', rf"\g<1>{self.page}\g<3>", response.request.url)
                yield SeleniumRequest(url=url,
                                      callback=self.parse,
                                      wait_time=5,
                                      script="window.scrollTo(0, document.body.scrollHeight);",
                                      wait_until=EC.visibility_of_element_located(
                                          (By.XPATH, './/i[contains(@class, "ui2-icon-loading")]'))
                                      )

